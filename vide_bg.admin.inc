<?php

/**
 * @file
 * Admin page callbacks for Vide module.
 */

/**
 * Vide main admin config form.
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 *
 * @return array
 *   An array representing the form definition.
 *
 * @ingroup forms
 */
function vide_bg_settings_form($form) {
  $form['fs_bg_files'] = array(
    '#type' => 'fieldset',
    '#title' => 'Background video/image files',
  );
  $form['fs_bg_files']['vide_bg_file_mp4'] = array(
    '#title' => t('mp4:'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('vide_bg_file_mp4', ''),
    '#upload_location' => 'public://vide_bg/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('mp4'),
      'file_validate_size' => array(50 * 1024 * 1024),
    ),
  );
  $form['fs_bg_files']['vide_bg_file_webm'] = array(
    '#title' => t('webm:'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('vide_bg_file_webm', ''),
    '#upload_location' => 'public://vide_bg/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('webm'),
      'file_validate_size' => array(50 * 1024 * 1024),
    ),
  );
  $form['fs_bg_files']['vide_bg_file_ogg'] = array(
    '#title' => t('ogg:'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('vide_bg_file_ogg', ''),
    '#upload_location' => 'public://vide_bg/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('ogv'),
      'file_validate_size' => array(50 * 1024 * 1024),
    ),
  );
  $form['fs_bg_files']['vide_bg_file_poster'] = array(
    '#title' => t('poster:'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://vide_bg/',
    '#default_value' => variable_get('vide_bg_file_poster', ''),
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg jpeg gif png svg bmp tiff pcx webp'),
      'file_validate_size' => array(50 * 1024 * 1024),
    ),
  );
  $form['fs_vide_bg_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Options',
  );
  $vide_type_bg_position = array(
    0 => 'Disabled',
    1 => 'Body',
    2 => 'Block',
    3 => 'Html element',
  );
  $form['fs_vide_bg_options']['vide_bg_type_bg_position'] = array(
    '#title' => t('Background:'),
    '#type' => 'radios',
    '#options' => $vide_type_bg_position,
    '#default_value' => variable_get('vide_bg_type_bg_position', 0),
  );
  $form['fs_vide_bg_options']['vide_bg_bg_block_id'] = array(
    '#type' => 'select',
    '#title' => t('Select block for background'),
    '#default_value' => variable_get('vide_bg_bg_block_id', '_none'),
    '#options' => vide_bg_block_load_all_blocks(),
    '#states' => array(
      'visible' => array(
        ':input[name="vide_bg_type_bg_position"]' => array('value' => 2),
      ),
    ),
  );
  $form['fs_vide_bg_options']['vide_bg_bg_html_element'] = array(
    '#type' => 'textfield',
    '#title' => t('Html identifier'),
    '#default_value' => variable_get('vide_bg_bg_html_element', ''),
    '#description' => t('Enter the id or class name of html element (for ex. #idname or .classname)'),
    '#states' => array(
      'visible' => array(
        ':input[name="vide_bg_type_bg_position"]' => array('value' => 3),
      ),
    ),
  );
  $form['fs_vide_bg_options']['vide_bg_volume'] = array(
    '#type' => 'textfield',
    '#title' => t('Volume'),
    '#default_value' => variable_get('vide_bg_volume', 1),
  );
  $form['fs_vide_bg_options']['vide_bg_playback_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Playback Rate'),
    '#default_value' => variable_get('vide_bg_playback_rate', 1),
  );

  $active = array(0 => 'No', 1 => 'Yes');
  $form['fs_vide_bg_options']['vide_bg_muted'] = array(
    '#type' => 'radios',
    '#options' => $active,
    '#title' => t('Muted'),
    '#default_value' => variable_get('vide_bg_muted', 1),
  );
  $form['fs_vide_bg_options']['vide_bg_loop'] = array(
    '#type' => 'radios',
    '#options' => $active,
    '#title' => t('Loop'),
    '#default_value' => variable_get('vide_bg_loop', 1),
  );
  $form['fs_vide_bg_options']['vide_bg_autoplay'] = array(
    '#type' => 'radios',
    '#options' => $active,
    '#title' => t('Autoplay'),
    '#default_value' => variable_get('vide_bg_autoplay', 1),
  );
  $form['fs_vide_bg_options']['vide_bg_position'] = array(
    '#type' => 'textfield',
    '#title' => t('Position'),
    '#default_value' => variable_get('vide_bg_position', '50% 50%'),
    '#description' => t('Similar to the CSS `background-position` property'),
  );
  $form['fs_vide_bg_options']['vide_bg_resizing'] = array(
    '#type' => 'radios',
    '#options' => $active,
    '#title' => t('Resizing'),
    '#default_value' => variable_get('vide_bg_resizing', 1),
  );
  $form['fs_vide_bg_options']['vide_bg_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#default_value' => variable_get('vide_bg_bg_color', 'transparent'),
    '#description' => t('Allow custom background-color for Vide div'),
  );
  $form['fs_vide_bg_options']['vide_bg_class_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Class name'),
    '#default_value' => variable_get('vide_bg_class_name', ''),
    '#description' => t('Add custom CSS class to Vide div'),
  );
  $form = system_settings_form($form);
  $form['#submit'][] = 'vide_bg_settings_form_submit';
  return $form;
}

/**
 * Form submission handler for vide_settings_form().
 *
 * @see vide_bg_settings_form()
 */
function vide_bg_settings_form_submit($form, &$form_state) {
  $file_ids = array(
    'vide_bg_file_mp4',
    'vide_bg_file_webm',
    'vide_bg_file_ogg',
    'vide_bg_file_poster',
  );
  foreach ($file_ids as $file_id) {
    $def_file = (!empty($form['fs_bg_files'][$file_id]['#default_value'])) ? $form['fs_bg_files'][$file_id]['#default_value'] : 0;

    // If we have been provided files, make sure to mark them as in use.
    if (isset($form_state['values'][$file_id]) && $form_state['values'][$file_id]) {

      /* If the uploaded file is different than the one we already have, then
      we need to remove the current file and replace it with the new one. */
      if (isset($def_file) && $def_file != $form_state['values'][$file_id]) {
        vide_bg_remove_managed_file($def_file, 'vide_bg');
        vide_bg_add_managed_file($form_state['values'][$file_id], 'vide_bg');
      }
      else {
        vide_bg_add_managed_file($form_state['values'][$file_id], 'vide_bg');
      }
    }
    else {
      vide_bg_remove_managed_file($def_file, 'vide_bg');
    }
    // Get extension of the vide_file_poster file.
    if ($file_id === 'vide_bg_file_poster') {
      $poster_file_extension = vide_bg_poster_file_extension($form_state['values'][$file_id]);
      variable_set('vide_bg_poster_type', $poster_file_extension);
    }
  }

  if ($form_state['values']['vide_bg_type_bg_position'] === "2") {
    $vide_bg_block_delta_array = explode('--', $form_state['values']['vide_bg_bg_block_id']);
    variable_set('vide_bg_bg_block_delta', $vide_bg_block_delta_array[2]);
  }
}

/**
 * Helper function for removing managed file.
 */
function vide_bg_remove_managed_file($managed_file, $which) {
  // Retrieve the old file's id.
  $file = $managed_file ? file_load($managed_file) : FALSE;
  if ($file) {
    file_usage_delete($file, 'vide_bg', $which, $file->fid);
    file_delete($file);
    drupal_set_message(t('The image @image_name was removed.', array('@image_name' => $file->filename)));
  }
}

/**
 * Helper function for adding managed file.
 */
function vide_bg_add_managed_file($managed_file, $which) {
  $count = db_query('SELECT `count` FROM {file_usage} WHERE fid=:fid', array('fid' => $managed_file))->fetchField();
  if (empty($count)) {
    $file = file_load($managed_file);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'vide_bg', $which, $managed_file);
    drupal_set_message(t('The image @image_name was added.', array('@image_name' => $file->filename)));
    unset($file);
  }
}

/**
 * Loads all blocks' information from the database.
 *
 * @return array
 *   An array of blocks grouped by region.
 */
function vide_bg_block_load_all_blocks() {
  $current_theme = variable_get('theme_default', 'none');
  $blocks = vide_bg_block_admin_display_prepare_blocks($current_theme);
  foreach ($blocks as $block) {
    $blocks_list["block--{$block['module']}--{$block['delta']}"] = $block['info'];
  }
  return $blocks_list;
}

/**
 * Get list of available blocks.
 */
function vide_bg_block_admin_display_prepare_blocks($theme) {
  return _block_rehash($theme);
}

/**
 * Get the file extension by fid.
 */
function vide_bg_poster_file_extension($fid) {
  if (!$fid) {
    return 'none';
  }
  $extension = 'detect';
  $file = file_load($fid);
  if (preg_match('/.+\.([A-Za-z]+)/', $file->filename, $output)) {
    $extension = drupal_strtolower($output[1]);
  }
  return $extension;
}
