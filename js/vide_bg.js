/**
 * @file
 * The main JS settings of Vide module.
 */

(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.videBg = {
    attach: function (context, settings) {
      var bgElementType = Drupal.settings.vide_bg_options.options.bgPosition;
      var bgHtmlElement = Drupal.settings.vide_bg_options.options.bgHtmlElement;
      var bgBlockId = Drupal.settings.vide_bg_options.options.bgBlockid;
      bgBlockId = bgBlockId.replace(/--|_/g, '-');
      var bgElement = '';
      switch (bgElementType) {
        case '1':
          bgElement = document.body;
          break;

        case '2':
          if (bgBlockId !== '') {
            bgElement = '#' + bgBlockId;
          }
          break;

        case '3':
          bgElement = bgHtmlElement;
          break;

      }
      $(bgElement).css('background', 'none');
      $(bgElement).css('z-index', '0');
      $(bgElement).vide(
        Drupal.settings.vide_bg_options.files, Drupal.settings.vide_bg_options.options
      );
    }
  };
}(jQuery, Drupal));
