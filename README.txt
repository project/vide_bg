CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The Vide module allows you to attach video background to your site.
It can be attached to the body element, to any Drupal blocks,
to the html element by "id" or "class" identifier. Note, video background
will be appear, if there are not other backgrounds on layers over the element
you choose.

REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://drupal.org/project/libraries)
 * jQuery Update (https://drupal.org/project/jquery_update)


INSTALLATION
------------

* Download the latest release of Vide plugin from
  https://github.com/VodkaBears/Vide/releases/

* Unzip and place the code in the appropriate directory:
    sites/all/libraries/vide_bg
    sites/all/libraries/vide_bg/dist/jquery.vide.min.js
    sites/all/libraries/vide_bg/dist/jquery.vide.js

* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
   Vide settings can be found at admin/config/media/vide_bg

TROUBLESHOOTING
---------------
   If you don't see video background after enabling and configuring vide module,
   make sure that your used css styles and regions do not overlap the video
   background.

MAINTAINERS
-----------
Current maintainers:
 * Aleksey Babko (inzor) - https://www.drupal.org/user/2779603
